import 'package:flutter/material.dart';
import '/model/movie.dart';

class AddMovie extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: Text("Add a movie"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: (){
            Navigator.pop(context,Movie.getFilmBidon());
          },
          child: Text("Ajouter un film bidon"),
        ),
      ),
    );
  }

}